import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  form: FormGroup;
  endereco: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.createForm();
    this.form.valueChanges.subscribe(console.info);
  }

  private createForm() {
    this.form = this.fb.group({
      endereco: this.fb.group({ logradouro: ['Rua XPTO'] }),
      laudo: this.fb.group({ lero: ['Meh'] })
    });
    this.endereco = this.form.get('endereco') as FormGroup;
  }

}
