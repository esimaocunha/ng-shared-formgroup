/* tslint:disable:component-class-suffix */
import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-endereco-partial',
  templateUrl: './endereco.partial.html'
})
export class EnderecoPartial {

  @Input()
  endereco: FormGroup;

}
